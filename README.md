# Traefik docker-compose project

This project is using [traefik](https://traefik.io/) / [docker-compose](https://docs.docker.com/compose/) / [docker](https://docs.docker.com/get-docker/) to set-up a ssl reverse proxy traefik instance linstening on the docker socket to add secure ingress for your docker powered applications.

More informations on traefik : [Official documentation](https://docs.traefik.io/)

## About

This docker-compose configuration start traefik on port `443/https` and `80/http`.

The traefik dashboard is on an internal `8080` port, dashboard access configuration and restriction is done via docker-compose _labels_ configurations.

### Traefik version compatibility

This docker-compose configuration and documentation is based on [traefik v1.7 branch](https://github.com/containous/traefik/tree/v1.7), you need to refer to the [documentation in version 1.7.x](https://docs.traefik.io/v1.7/).

## Installation and configuration

1. Clone the repo
    ```bash
    # Clone the repository
    git clone https://gitlab.com/botux-fr/docker/docker-service-traefik.git
    cd docker-service-traefik
    ```
1. Copy .env.dist to .env and edit your settings.
    ```bash
    # Copy env.dist
    cp .env.dist .env
    # Edit .env
    nano .env
    ```

    ```env
    TRAEFIK_VERSION=1.7-alpine
    # Domain to use for traefik dashboard/api/metrics https.
    # you can use reverse-proxy.XX.XX.XX.XX.xip.io with XX.XX.XX.XX is your external ip.
    TRAEFIK_DOMAIN=traefik.127.0.0.1.xip.io
    # IP/range, comma separated. If empty no restrictions.
    TRAEFIK_WHITELIST=
    ```
1. We need to manually create the external network to be able to stop and upgrade traefik without removing the network if other containers are connected to it.
   - Basic configurations : 
      ```bash
      docker network create reverse-proxy
      ```
   - Advance network configuration : *if you need a specific ip for the traefik network (you might need to comply with some internal routing policies), you can set up the network with a command like* : 
      ```bash
      docker network create --driver=bridge --subnet=172.18.0.0/24 --gateway 172.18.0.1 -o "com.docker.network.bridge.name"="br-reverseproxy" reverse-proxy
      ```
1. Configure account information for let's encrypt ssl certificat. You need to edit [traefik/traefik.toml#L36](traefik/traefik.toml#L36) and change the mail address.
      ```bash
      # Edit traefik/traefik.toml to change Acme email address
      nano traefik/traefik.toml
      ```
1. Validate your configuration.
   ```bash
   # Validate configuration
   docker-compose config
   ```

### Without let's encrypt : Self signed certificate

If you want to use a self signed certicicate (for dev only !), you can use the following configuration : 

1. Generate your self signed certificate :
  1. With openssl, you can generate a certificate for localhost, traefik.127.0.0.1.xip.io and *.docker.127.0.0.1.xip.io with the command :
     ```bash
     openssl req -x509 -out certificates/localhost.crt -keyout certificates/localhost.key \
     -newkey rsa:2048 -nodes -sha256 \
     -subj '/CN=localhost' -extensions EXT -config <( \
     printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName = @alt_names\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth\n[alt_names]\nDNS.1 = localhost\nDNS.2 = traefik.127.0.0.1.xip.io\nDNS.3 = *.docker.127.0.0.1.xip.io\n")
     ```
  1. Manually, put your certificates in `./certificates/`
1. Run traefik with the complementary docker-compose file [compose/localhost-ssl.yml](compose/localhost-ssl.yml) :
   ```bash
   docker-compose -f docker-compose.yml -f compose/localhost-ssl.yml up -d
   ```
1. You can check traefik dashboard on https://traefik.127.0.0.1.xip.io/.
1. You can run any containers on the reverse proxy by adding :
  - docker run : `--network reverse-proxy` to the run command.
   Exemple : `docker run --rm -ti --network reverse-proxy --name www nginx:alpine` will be accesible via https://www.docker.127.0.0.1.xip.io/.
  - docker-compose.yml : add the network globally and on the application.
    ```yml
    networks:
      reverse-proxy:
        name: reverse-proxy
        external: true

    services:
      app:
        image: nginx:alpine
        networks:
          - reverse-proxy
    ```


## Running

You can start the stack with : 

    docker-compose up -d
    # Show logs :
    docker-compose logs -f

## Connect other services/app to traefik

To setup a new traefik frontend/backend with your app, simply create a`docker-compose.yml` with :

```yml
version: "3.7"

networks:
  reverse-proxy:
    name: reverse-proxy
    external: true

services:
  app:
    image: nginx
    restart: always
    networks:
      - reverse-proxy
    labels:
      - "traefik.docker.network=reverse-proxy"
      - "traefik.enable=true"
      - "traefik.port=${APP_PORT:-80}"
      - "traefik.backend=${APP_NAME:-app}"
      - "traefik.frontend.passHostHeader=true"
      - "traefik.frontend.rule=Host:${APP_DOMAIN:-app.127.0.0.1.xip.io}"
      - "traefik.frontend.whiteList.sourceRange=${APP_WHITELIST:-}"
      - "traefik.frontend.auth.basic.users=${APP_BASIC_AUTH:-}"
```

Add an .env file to your docker-compose stack :

```env
APP_PORT=80
APP_NAME=app
APP_DOMAIN=app.XX.XX.XX.XX.xip.io
```

More informations on [how to configure docker service via labels in traefik](https://docs.traefik.io/configuration/backends/docker/#labels-overriding-default-behavior)


## External documentation

- Traefik |  More informations on [traefik dashboard](https://docs.traefik.io/v1.7/configuration/api/).
- Docker-compose | [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)
